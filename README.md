# README #

This is a simple script to integrate with the Appointments+ and Marketpress plugins. 
It redirects the user to the cart after they have confirmed their appointment.
It relates to the issues raised on https://premium.wpmudev.org/forums/topic/appointments-go-directly-to-cart-wo-clicking-proceed-link

You can use this to avoid the adjustments to the plugins, mentioned in the topic thread, from being overwritten.

This doesn't change the default behaviour of the plugins, but triggers a redirect as soon as the 'Please, proceed to checkout.' message appears.
<?php 
/*
Plugin Name: Appointments+ & Marketplace Cart Redirect
Description: Redirects to the MP cart for payment once an appointment has been created
Plugin URI: https://bitbucket.org/LeodanDesign/wpmu-appointments-marketplace-cart-redirect
Version: 0.1
Author: Leodan:Design
Author URI: https://leodandesign.co.uk
*/

function appointments_enqueue_override(){
    if ( is_plugin_active('appointments/appointments.php') && is_plugin_active('marketpress/marketpress.php') ) {
        wp_localize_script('mp-frontend', 'apt_mp_frontend_urls', array('wpmu_mp_cart' => esc_url( mp_cart_link( false, true ) ) ) );
        $script = 'jQuery(document).ready(function($) { $(document).on("app-confirmation-response_received", function (e, response) { if (response && response.mp && 1 == response.mp) { window.location = apt_mp_frontend_urls.wpmu_mp_cart; } }); });';
        wp_add_inline_script('mp-frontend', $script);
    }
}
add_action('wp_enqueue_scripts', 'appointments_enqueue_override', 99);